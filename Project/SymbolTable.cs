﻿using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class SymbolTable
    {
        Dictionary<string, (DataType Type, object Value)> memory = new Dictionary<string, (DataType Type, object Value)>();

        public void Add(IToken variable, DataType type)
        {
            var name = variable.Text.Trim();
            if (memory.ContainsKey(name))
            {
                Errors.ReportError(variable, $"Variable {name} was already declared.");
            }
            else
            {
                if (type == DataType.Int)
                    memory.Add(name, (type, 0));
                else if (type == DataType.Float)
                    memory.Add(name, (type, (float)0));
                else if (type == DataType.String)
                    memory.Add(name, (type, ""));
                else if (type == DataType.Bool)
                    memory.Add(name, (type, false));
                else
                {
                    Errors.ReportError("Variable " + name + " is unknown type.");
                }
            }
        }
        public (DataType Type, object Value) this[IToken variable]
        {
            get
            {
                var name = variable.Text.Trim();
                if (memory.ContainsKey(name))
                {
                    return memory[name];
                }
                else
                {
                    Errors.ReportError(variable, $"Variable {name} was NOT declared.");
                    return (DataType.Error, 0);
                }
            }
            set
            {
                var name = variable.Text.Trim();
                memory[name] = value;
            }
        }
    }
}
