﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class Interpreter
    {
        private Dictionary<int, int> labels = new Dictionary<int, int>();
        private List<string> code = new List<string>();
        Dictionary<string, (DataType type, object value)> memory = new Dictionary<string, (DataType type, object value)>();
        Stack<(DataType type, object value)> stack = new Stack<(DataType type, object value)>();

        int actual = 0;
        public Interpreter(string filename)
        {
            if (!File.Exists(filename))
            {
                return;
            }
            var input = File.ReadAllLines(filename);
            int i = 0;
            foreach (var line in input)
            {
                code.Add(line);
                if (line.StartsWith("label"))
                {
                    var split = line.Split(' ');
                    var index = int.Parse(split[1]);

                    labels.Add(index, i);
                }
                i++;
            }
        }
        private void Load(string name)
        {
            if (memory.ContainsKey(name))
            {
                stack.Push(memory[name]);
            }
            else
                throw new Exception($"Variable {name} was not initialized");
            actual++;
        }
        private void Save(string name)
        {
            var value = stack.Pop();
            memory[name] = value;
            actual++;
        }
        private void Print(string nString)
        {
            int n = int.Parse(nString);
            List<object> items = new List<object>();
            for (int i = 0; i < n; i++)
            {
                items.Add(stack.Pop().value);
            }
            items.Reverse();
            foreach (var item in items)
            {
                Console.Write(item);
            }
            Console.Write("\n");
            actual++;
        }
        private void Read(string type)
        {
            string value = Console.ReadLine();
            switch (type)
            {
                case "I":
                    {
                        int output;
                        if (int.TryParse(value, out output))
                            stack.Push((DataType.Int, output));
                        else
                            throw new Exception($"Read value isnt of expected type");
                    }
                    break;
                case "F":
                    {
                        float output;
                        if (float.TryParse(value, out output))
                            stack.Push((DataType.Float, output));
                        else
                            throw new Exception($"Read value isnt of expected type");
                    }
                    break;
                case "B":
                    {
                        if (value.Equals("true"))
                            stack.Push((DataType.Bool, true));
                        else if (value.Equals("false"))
                            stack.Push((DataType.Bool, false));
                        else
                            throw new Exception($"Read value isnt of expected type");

                    }
                    break;
                case "S":
                    {
                        stack.Push((DataType.String, value.Replace("\"", String.Empty)));
                    }
                    break;
            }
            actual++;
        }
        private void Push(string[] command)
        {

            var type = command[1];
            var value = command[2];
            if (type == "S")
                for (int i = 3; i < command.Length; i++)
                    value += " " + command[i];
            switch (type)
            {
                case "I":
                    {
                        int output;
                        if (int.TryParse(value, out output))
                            stack.Push((DataType.Int, output));
                        else
                            throw new Exception($"Pushed value isnt of expected type");
                    }
                    break;
                case "F":
                    {
                        float output;
                        if (float.TryParse(value, out output))
                            stack.Push((DataType.Float, output));
                        else
                            throw new Exception($"Pushed value isnt of expected type");
                    }
                    break;
                case "B":
                    {
                        if (value.Equals("true"))
                            stack.Push((DataType.Bool, true));
                        else if (value.Equals("false"))
                            stack.Push((DataType.Bool, false));
                        else
                            throw new Exception($"Pushed value isnt of expected type");

                    }
                    break;
                case "S":
                    {
                        stack.Push((DataType.String, value.Replace("\"", String.Empty)));
                    }
                    break;
            }
            actual++;
        }
        private void Itof()
        {
            var value = stack.Pop();

            if (value.type == DataType.Int)
                stack.Push((DataType.Float, (float)(Convert.ToInt32(value.value))));
            else
                stack.Push((DataType.Float, value.value));
            actual++;
        }
        private void Not()
        {
            (DataType type, object value) value = stack.Pop();
            stack.Push((DataType.Bool, !(bool)value.value));
            actual++;
        }
        private void Eq()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.Int:
                    {
                        stack.Push((DataType.Bool, (int)leftValue.value == (int)rightValue.value));
                    }
                    break;
                case DataType.String:
                    {
                        stack.Push((DataType.Bool, (string)leftValue.value == (string)rightValue.value));
                    }
                    break;
                case DataType.Float:
                    {
                        stack.Push((DataType.Bool, (float)leftValue.value == (float)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void Lt()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.Int:
                    {
                        stack.Push((DataType.Bool, (int)leftValue.value < (int)rightValue.value));
                    }
                    break;
                case DataType.Float:
                    {
                        stack.Push((DataType.Bool, (float)leftValue.value > (float)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void Gt()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.Int:
                    {
                        stack.Push((DataType.Bool, (int)leftValue.value > (int)rightValue.value));
                    }
                    break;
                case DataType.Float:
                    {
                        stack.Push((DataType.Bool, (float)leftValue.value > (float)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void Or()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.Bool:
                    {
                        stack.Push((DataType.Bool, (bool)leftValue.value || (bool)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void And()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.Bool:
                    {
                        stack.Push((DataType.Bool, (bool)leftValue.value && (bool)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void Concat()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.String:
                    {
                        stack.Push((DataType.String, (string)leftValue.value + (string)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void Uminus()
        {
            (DataType type, object value) value = stack.Pop();
            switch (value.type)
            {
                case DataType.Int:
                    {
                        stack.Push((DataType.Int, -(int)value.value));
                    }
                    break;
                case DataType.Float:
                    {
                        stack.Push((DataType.Float, -(float)value.value));
                    }
                    break;
            }
            actual++;
        }
        private void Mod()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();

            stack.Push((DataType.Int, (int)leftValue.value % (int)rightValue.value));
            actual++;
        }
        private void Div()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.Float:
                    {
                        stack.Push((DataType.Float, (float)leftValue.value / (float)rightValue.value));
                    }
                    break;
                case DataType.Int:
                    {
                        stack.Push((DataType.Int, (int)leftValue.value / (int)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void Mul()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.Float:
                    {
                        stack.Push((DataType.Float, (float)leftValue.value * (float)rightValue.value));
                    }
                    break;
                case DataType.Int:
                    {
                        stack.Push((DataType.Int, (int)leftValue.value * (int)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void Add()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.Float:
                    {
                        stack.Push((DataType.Float, (float)leftValue.value + (float)rightValue.value));
                    }
                    break;
                case DataType.Int:
                    {
                        stack.Push((DataType.Int, (int)leftValue.value + (int)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void Sub()
        {
            (DataType type, object value) rightValue = stack.Pop();
            (DataType type, object value) leftValue = stack.Pop();
            switch (leftValue.type)
            {
                case DataType.Float:
                    {
                        stack.Push((DataType.Float, (float)leftValue.value - (float)rightValue.value));
                    }
                    break;
                case DataType.Int:
                    {
                        stack.Push((DataType.Int, (int)leftValue.value - (int)rightValue.value));
                    }
                    break;
            }
            actual++;
        }
        private void Jmp(string indexString)
        {
            var index = int.Parse(indexString);
            actual = labels[index];
        }
        private void Fjmp(string indexString)
        {
            var value = stack.Pop();
            if ((bool)value.value)
            {
                actual++;
            }
            else
            {
                var index = int.Parse(indexString);
                actual = labels[index];
            }
        }
        private void Label()
        {
            actual++;
        }
        private void Pop()
        {
            stack.Pop();
            actual++;
        }
        public void Run()
        {

            while (actual < code.Count)
            {
                Type typeClass = this.GetType();
                var command = code[actual].Split(" ");
                if (command[0].Equals("push"))
                {
                    Push(command);
                }
                else if (command[0].Equals("label"))
                {
                    Label();
                }
                else if (command.Length == 1)
                {

                    if (typeClass != null)
                    {

                        string methodName = StringExtensions.FirstCharToUpper(command[0]);
                        MethodInfo method = typeClass.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic);

                        if (method != null)
                        {
                            method.Invoke(this, null);
                        }
                    }
                }
                else if (command.Length == 2)
                {

                    string methodName = StringExtensions.FirstCharToUpper(command[0]);
                    MethodInfo method = typeClass.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(string) }, null);

                    if (method != null)
                    {
                        method.Invoke(this, new object[] { command[1] });
                    }
                }
            }
            File.Delete("output.txt");
        }

    }
}
