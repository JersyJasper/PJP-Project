﻿using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using System;
using System.Globalization;
using System.IO;
using System.Threading;

namespace Project
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            var fileName = "input.txt";
            var inputFile = new StreamReader(fileName);
            AntlrInputStream input = new AntlrInputStream(inputFile);
            ExprLexer lexer = new ExprLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            ExprParser parser = new ExprParser(tokens);

            parser.AddErrorListener(new VerboseErrorListener());

            IParseTree tree = parser.program();

            if (parser.NumberOfSyntaxErrors == 0)
            {
                ParseTreeWalker walker = new ParseTreeWalker();
                walker.Walk(new CodeGeneratorListener(), tree);

                Interpreter interpreter = new Interpreter("output.txt");
                interpreter.Run();
            }



        }
    }
}