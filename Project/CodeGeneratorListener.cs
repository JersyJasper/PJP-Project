﻿using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class CodeGeneratorListener : ExprBaseListener
    {
        ParseTreeProperty<string> code = new ParseTreeProperty<string>();
        ParseTreeProperty<DataType> values = new ParseTreeProperty<DataType>();
        SymbolTable symbolTable = new SymbolTable();

        int label = -1;
        public override void ExitProgram([NotNull] ExprParser.ProgramContext context)
        {
            if (Errors.NumberOfErrors != 0)
            {
                Errors.PrintAndClearErrors();
                return;
            }

            StringBuilder sb = new StringBuilder();
            foreach (var statement in context.statement())
            {
                sb.Append(code.Get(statement));
            }
            File.WriteAllText("output.txt", sb.ToString());
            Console.WriteLine(sb.ToString());
        }
        public override void ExitEmptyStatement([NotNull] ExprParser.EmptyStatementContext context)
        {
            base.ExitEmptyStatement(context);
        }

        public override void ExitBlockOfStatements([NotNull] ExprParser.BlockOfStatementsContext context)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var statement in context.statement())
            {
                sb.Append(code.Get(statement));
            }
            code.Put(context, sb.ToString());
        }

        public override void ExitDataType([NotNull] ExprParser.DataTypeContext context)
        {
            if (context.type.Text == "int")
            {
                values.Put(context, DataType.Int);
                code.Put(context, "push I 0\n");
            }
            else if (context.type.Text == "float")
            {
                values.Put(context, DataType.Float);
                code.Put(context, "push F 0.0\n");
            }
            else if (context.type.Text == "string")
            {
                values.Put(context, DataType.String);
                code.Put(context, "push S \"\"\n");
            }
            else if (context.type.Text == "bool")
            {
                values.Put(context, DataType.Bool);
                code.Put(context, "push B false\n");
            }
            else
            {
                Errors.ReportError("ERROR: Initializing incorrect DataType");
                return;
            }

        }
        public override void ExitDeclaration([NotNull] ExprParser.DeclarationContext context)
        {
            var type = code.Get(context.dataType());
            var typeValue = values.Get(context.dataType());

            StringBuilder sb = new StringBuilder();
            foreach (var identifier in context.IDENTIFIER())
            {
                sb.Append(type);
                sb.Append("save ");
                sb.AppendLine(identifier.GetText());
                symbolTable.Add(identifier.Symbol, typeValue);
            }
            code.Put(context, sb.ToString());
        }

        public override void ExitIfElse([NotNull] ExprParser.IfElseContext context)
        {
            var typeValue = values.Get(context.expr());
            if (typeValue != DataType.Bool) 
            {
                Errors.ReportError(context.IF().Symbol, "ERROR: Condition in IF statement must be bool");
                return;
            }

            var condition = code.Get(context.expr());
            int fjumpLabel = label++;
            int positiveEnd = label++;
            string positiveBranch = code.Get(context.pos);
            string negativeBranch = String.Empty;
            if (context.neg != null)
            {
                code.Get(context.neg);
            }
            StringBuilder sb = new StringBuilder();
            sb.Append(condition);
            sb.Append("fjmp ").AppendLine(fjumpLabel.ToString());
            sb.Append(positiveBranch.ToString());
            sb.Append("jmp ").AppendLine(positiveEnd.ToString());
            sb.Append("label ").AppendLine(fjumpLabel.ToString());
            sb.Append(negativeBranch);
            sb.Append("label ").AppendLine(positiveEnd.ToString());

            code.Put(context, sb.ToString());
        }
        public override void ExitWhile([NotNull] ExprParser.WhileContext context)
        {
            var typeValue = values.Get(context.expr());
            if (typeValue != DataType.Bool)
            {
                Errors.ReportError(context.WHILE().Symbol, "ERROR: Condition in WHILE statement must be bool");
                return;
            }

            var condition = code.Get(context.expr());

            var startLabel = label++;
            var endLabel = label++;

            StringBuilder block = new StringBuilder();
            block.Append(code.Get(context.statement()));
            block.Append("jmp ").AppendLine(startLabel.ToString());

            StringBuilder value = new StringBuilder();
            value.Append("label ").AppendLine(startLabel.ToString());
            value.Append(condition).Append("fjmp ").AppendLine(endLabel.ToString());
            value.Append(block.ToString());
            value.Append("label ").AppendLine(endLabel.ToString());

            code.Put(context, value.ToString());
        }

        public override void ExitReadStatement([NotNull] ExprParser.ReadStatementContext context)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var identifier in context.IDENTIFIER())
            {
                var variableValue = symbolTable[identifier.Symbol];

                switch (variableValue.Type)
                {
                    case DataType.Bool:
                        {
                            sb.AppendLine("read B");
                        }
                        break;
                    case DataType.Int:
                        {
                            sb.AppendLine("read I");
                        }
                        break;
                    case DataType.Float:
                        {
                            sb.AppendLine("read F");
                        }
                        break;
                    case DataType.String:
                        {
                            sb.AppendLine("read S");
                        }
                        break;
                    default:
                        Errors.ReportError("ERROR: Reading incorrect DataType");
                        return;
                }
                sb.Append("save ").AppendLine(identifier.Symbol.Text);
            }
            code.Put(context, sb.ToString());
        }

        public override void ExitWriteStatement([NotNull] ExprParser.WriteStatementContext context)
        {
            StringBuilder sb = new StringBuilder();
            int count = 0;
            foreach (var expr in context.expr())
            {
                var valueOfExpr = values.Get(expr);
                if (valueOfExpr != DataType.String && valueOfExpr != DataType.Int && valueOfExpr != DataType.Float && valueOfExpr != DataType.Bool)
                {
                    Errors.ReportError(context.WRITE().Symbol, "ERROR: Writing Incorrect Type");
                }
                var exprCode = code.Get(expr);
                sb.Append(exprCode);
                count++;
            }
            sb.Append("print ").AppendLine(count.ToString());
            code.Put(context, sb.ToString());
        }

        public override void ExitPrintExpr([NotNull] ExprParser.PrintExprContext context)
        {
            code.Put(context, code.Get(context.expr()));
        }

        public override void ExitId([NotNull] ExprParser.IdContext context)
        {

            code.Put(context, "load " + context.IDENTIFIER().GetText() + "\n");
            values.Put(context, symbolTable[context.IDENTIFIER().Symbol].Type);
        }
        public override void ExitInt([NotNull] ExprParser.IntContext context)
        {
            if (Int32.TryParse(context.INT().GetText(), out int value))
            {
                code.Put(context, $"push I {value}\n");
                values.Put(context, DataType.Int);
            }
            else
            {
                Errors.ReportError(context.INT().Symbol,"ERROR: Incorrect value for dataType Int");
                return;
            }

        }
        public override void ExitFloat([NotNull] ExprParser.FloatContext context)
        {
            if (float.TryParse(context.FLOAT().GetText(), out float value))
            {
                code.Put(context, $"push F {value}\n");
                values.Put(context, DataType.Float);
            }
            else
            {
                Errors.ReportError(context.FLOAT().Symbol, "ERROR: Incorrect value for dataType Float");
                return;
            }
        }
        public override void ExitBool([NotNull] ExprParser.BoolContext context)
        {
            var value = context.GetText();

            if (value == "true" || value == "false" || value == "0" || value == "1")
            {
                var convertedValue = Convert.ToBoolean(value);
                code.Put(context, $"push B {convertedValue.ToString().ToLower()}\n");
                values.Put(context, DataType.Bool);
            }
            else
            {
                Errors.ReportError(context.BOOL().Symbol, "ERROR: Incorrect value for dataType Bool");
                return;
            }

        }
        public override void ExitString([NotNull] ExprParser.StringContext context)
        {
            var value = context.STRING().GetText();
            code.Put(context, $"push S {value}\n");
            values.Put(context, DataType.String);
        }

        public override void ExitParentheses([NotNull] ExprParser.ParenthesesContext context)
        {
            code.Put(context, code.Get(context.expr()));
            values.Put(context, values.Get(context.expr()));
        }

        public override void ExitAddSub([NotNull] ExprParser.AddSubContext context)
        {
            var left = code.Get(context.expr()[0]);
            var right = code.Get(context.expr()[1]);
            var leftValue = values.Get(context.expr()[0]);
            var rightValue = values.Get(context.expr()[1]);

            var containsOnlyFloat = (leftValue == DataType.Float && rightValue == DataType.Float) ? true : false;
            var containsOnlyInt = (leftValue == DataType.Int && rightValue == DataType.Int) ? true : false;
            var containsFloat = (leftValue == DataType.Float || rightValue == DataType.Float) ? true : false;
            var containsBoolean = (leftValue == DataType.Bool || rightValue == DataType.Bool) ? true : false;
            var containsString = (leftValue == DataType.String || rightValue == DataType.String) ? true : false;
            var containsStringOrBoolean = (containsBoolean || containsString) ? true : false;

            switch (context.op.Type)
            {
                case ExprParser.ADD:
                    {
                        if (containsOnlyFloat || containsOnlyInt)
                        {
                            values.Put(context, leftValue);
                            code.Put(context, left + right + "add\n");
                        }
                        else if (containsFloat && !containsStringOrBoolean)
                        {
                            values.Put(context, DataType.Float);
                            if (leftValue == DataType.Float)
                            {
                                code.Put(context, left + right + "itof\n" + "add\n");
                            }
                            else
                            {
                                code.Put(context, left + "itof\n" + right + "add\n");
                            }
                        }
                        else
                        {
                            Errors.ReportError(context.ADD().Symbol, "ERROR: Incorrect type in ADD");
                            return;
                        }
                    }
                    break;
                case ExprParser.SUB:
                    {
                        if (containsOnlyFloat || containsOnlyInt)
                        {
                            values.Put(context, leftValue);
                            code.Put(context, left + right + "sub\n");
                        }
                        else if (containsFloat && !containsStringOrBoolean)
                        {
                            values.Put(context, DataType.Float);
                            if (leftValue == DataType.Float)
                            {
                                code.Put(context, left + right + "itof\n" + "sub\n");
                            }
                            else
                            {
                                code.Put(context, left + "itof\n" + right + "sub\n");
                            }
                        }
                        else
                        {
                            Errors.ReportError(context.SUB().Symbol, "ERROR: Incorrect type in SUB");
                            return;
                        }
                    }
                    break;
            }
        }


        public override void ExitMulDiv([NotNull] ExprParser.MulDivContext context)
        {
            var left = code.Get(context.expr()[0]);
            var right = code.Get(context.expr()[1]);
            var leftValue = values.Get(context.expr()[0]);
            var rightValue = values.Get(context.expr()[1]);

            var containsOnlyFloat = (leftValue == DataType.Float && rightValue == DataType.Float) ? true : false;
            var containsOnlyInt = (leftValue == DataType.Int && rightValue == DataType.Int) ? true : false;
            var containsFloat = (leftValue == DataType.Float || rightValue == DataType.Float) ? true : false;
            var containsBoolean = (leftValue == DataType.Bool || rightValue == DataType.Bool) ? true : false;
            var containsString = (leftValue == DataType.String || rightValue == DataType.String) ? true : false;
            var containsStringOrBoolean = (containsBoolean || containsString) ? true : false;

            switch (context.op.Type)
            {
                case ExprParser.MUL:
                    {
                        if (containsOnlyFloat || containsOnlyInt)
                        {
                            values.Put(context, leftValue);
                            code.Put(context, left + right + "mul\n");
                        }
                        else if (containsFloat && !containsStringOrBoolean)
                        {
                            values.Put(context, DataType.Float);
                            if (leftValue == DataType.Float)
                            {
                                code.Put(context, left + right + "itof\n" + "mul\n");
                            }
                            else
                            {
                                code.Put(context, left + "itof\n" + right + "mul\n");
                            }
                        }
                        else
                        {
                            Errors.ReportError(context.MUL().Symbol,"ERROR: Incorrect type in MUL");
                            return;
                        }
                    }
                    break;
                case ExprParser.DIV:
                    {
                        if (containsOnlyFloat || containsOnlyInt)
                        {
                            values.Put(context, leftValue);
                            code.Put(context, left + right + "div\n");
                        }
                        else if (containsFloat && !containsStringOrBoolean)
                        {
                            values.Put(context, DataType.Float);
                            if (leftValue == DataType.Float)
                            {
                                code.Put(context, left + right + "itof\n" + "div\n");
                            }
                            else
                            {
                                code.Put(context, left + "itof\n" + right + "div\n");
                            }
                        }
                        else
                        {
                            Errors.ReportError(context.DIV().Symbol, "ERROR: Incorrect type in DIV");
                            return;
                        }
                    }
                    break;
            }
        }

        public override void ExitMod([NotNull] ExprParser.ModContext context)
        {
            var left = code.Get(context.expr()[0]);
            var right = code.Get(context.expr()[1]);
            var leftValue = values.Get(context.expr()[0]);
            var rightValue = values.Get(context.expr()[1]);

            if (leftValue != DataType.Int || rightValue != DataType.Int)
            {
                Errors.ReportError(context.MOD().Symbol, "ERROR: Incorrect type in MOD");
                return;
            }

            values.Put(context, DataType.Int);
            code.Put(context, left + right + "mod\n");
        }
        public override void ExitConcat([NotNull] ExprParser.ConcatContext context)
        {
            var left = code.Get(context.expr()[0]);
            var right = code.Get(context.expr()[1]);
            var leftValue = values.Get(context.expr()[0]);
            var rightValue = values.Get(context.expr()[1]);

            if (!(leftValue == DataType.String && rightValue == DataType.String))
            {
                Errors.ReportError(context.CONCAT().Symbol, "ERROR: Incorrect type (not string) in CONCAT");
                return;
            }

            values.Put(context, DataType.String);
            code.Put(context, left + right + "concat\n");

        }

        public override void ExitRelation([NotNull] ExprParser.RelationContext context)
        {
            var left = code.Get(context.expr()[0]);
            var right = code.Get(context.expr()[1]);
            var leftValue = values.Get(context.expr()[0]);
            var rightValue = values.Get(context.expr()[1]);

            var containsOnlyFloat = (leftValue == DataType.Float && rightValue == DataType.Float) ? true : false;
            var containsFloat = (leftValue == DataType.Float || rightValue == DataType.Float) ? true : false;
            var containsString = (leftValue == DataType.String || rightValue == DataType.String) ? true : false;
            var containsBoolean = (leftValue == DataType.Bool || rightValue == DataType.Bool) ? true : false;

            switch (context.op.Type)
            {
                case ExprParser.LES:
                    {
                        if (containsString || containsBoolean)
                        {
                            Errors.ReportError(context.LES().Symbol, $"ERROR:{leftValue} used in LES");
                            return;
                        }

                        if (containsOnlyFloat)
                        {
                            values.Put(context, DataType.Bool);
                            code.Put(context, left + right + "lt\n");
                        }
                        else if (containsFloat)
                        {
                            values.Put(context, DataType.Bool);
                            if (leftValue == DataType.Float)
                            {
                                code.Put(context, left + right + "itof\n" + "lt\n");
                            }
                            else
                            {
                                code.Put(context, left + "itof\n" + right + "lt\n");
                            }
                        }
                        else
                        {
                            values.Put(context, DataType.Bool);
                            code.Put(context, left + right + "lt\n");
                        }
                    }
                    break;
                case ExprParser.GRE:
                    {
                        if (containsString || containsBoolean)
                        {
                            Errors.ReportError(context.GRE().Symbol, $"ERROR:{leftValue} used in GRE");
                            return;
                        }
                        else if (containsOnlyFloat)
                        {
                            values.Put(context, DataType.Bool);
                            code.Put(context, left + right + "gt\n");
                        }
                        else if (containsFloat)
                        {
                            values.Put(context, DataType.Bool);
                            if (leftValue == DataType.Float)
                            {
                                code.Put(context, left + right + "itof\n" + "gt\n");
                            }
                            else
                            {
                                code.Put(context, left + "itof\n" + right + "gt\n");
                            }
                        }
                        else
                        {
                            values.Put(context, DataType.Bool);
                            code.Put(context, left + right + "gt\n");
                        }

                    }
                    break;
            }
        }

        public override void ExitComparison([NotNull] ExprParser.ComparisonContext context)
        {
            var left = code.Get(context.expr()[0]);
            var right = code.Get(context.expr()[1]);
            var leftValue = values.Get(context.expr()[0]);
            var rightValue = values.Get(context.expr()[1]);

            var containsOnlyFloat = (leftValue == DataType.Float && rightValue == DataType.Float) ? true : false;
            var containsFloat = (leftValue == DataType.Float || rightValue == DataType.Float) ? true : false;
            var containsOnlyString = (leftValue == DataType.String && rightValue == DataType.String) ? true : false;
            var containsOnlyInt = (leftValue == DataType.Int && rightValue == DataType.Int) ? true : false;

            switch (context.op.Type)
            {
                case ExprParser.EQ:
                    {
                        if (containsOnlyFloat)
                        {
                            values.Put(context, DataType.Bool);
                            code.Put(context, left + right + "eq\n");
                        }
                        else if (containsFloat)
                        {
                            values.Put(context, DataType.Bool);
                            if (leftValue == DataType.Float)
                            {
                                code.Put(context, left + right + "itof\n" + "eq\n");
                            }
                            else
                            {
                                code.Put(context, left + "itof\n" + right + "eq\n");
                            }
                        }
                        else if (containsOnlyString)
                        {
                            values.Put(context, DataType.Bool);
                            code.Put(context, left + right + "eq\n");
                        }
                        else if (containsOnlyInt)
                        {
                            values.Put(context, DataType.Bool);
                            code.Put(context, left + right + "eq\n");
                        }
                        else
                        {
                            Errors.ReportError(context.EQ().Symbol,"ERROR: Incorrect types in EQ");
                            return;
                        }
                    }
                    break;
                case ExprParser.NEQ:
                    {
                        if (containsOnlyFloat)
                        {
                            values.Put(context, DataType.Bool); ;
                            code.Put(context, left + right + "eq\n" + "not\n");
                        }
                        else if (containsFloat)
                        {
                            values.Put(context, DataType.Bool);
                            if (leftValue == DataType.Float)
                            {
                                code.Put(context, left + right + "itof\n" + "eq\n" + "not\n");
                            }
                            else
                            {
                                code.Put(context, left + "itof\n" + right + "eq\n" + "not\n");
                            }
                        }
                        else if (containsOnlyString)
                        {
                            values.Put(context, DataType.Bool);
                            code.Put(context, left + right + "eq\n" + "not\n");
                        }
                        else if (containsOnlyInt)
                        {
                            values.Put(context, DataType.Bool);
                            code.Put(context, left + right + "eq\n" + "not\n");
                        }
                        else
                        {
                            Errors.ReportError(context.NEQ().Symbol, "Incorrect types in NEQ");
                            return;
                        }
                    }
                    break;
            }
        }

        public override void ExitLogicalAndOr([NotNull] ExprParser.LogicalAndOrContext context)
        {
            var left = code.Get(context.expr()[0]);
            var right = code.Get(context.expr()[1]);
            var leftValue = values.Get(context.expr()[0]);
            var rightValue = values.Get(context.expr()[1]);

            if (!(leftValue == DataType.Bool && rightValue == DataType.Bool))
            {
                Errors.ReportError("Incorrect types used in AND or OR - '" + context.Parent.GetText() + "'");
                return;
            }

            switch (context.op.Type)
            {
                case ExprParser.AND:
                    {
                        values.Put(context, DataType.Bool);
                        code.Put(context, left + right + "and\n");
                    }
                    break;
                case ExprParser.OR:
                    {
                        values.Put(context, DataType.Bool);
                        code.Put(context, left + right + "or\n");
                    }
                    break;
            }
        }

        public override void ExitUnaryMinus([NotNull] ExprParser.UnaryMinusContext context)
        {
            var operandValue = values.Get(context.expr());
            var operand = code.Get(context.expr());

            if ((operandValue != DataType.Int) && (operandValue != DataType.Float))
            {
                Errors.ReportError("ERROR: Incorrect Type for UnaryMinus");
            }

            values.Put(context, operandValue);
            code.Put(context, operand + "uminus\n");
        }

        public override void ExitNegation([NotNull] ExprParser.NegationContext context)
        {
            var operandValue = values.Get(context.expr());
            var operand = code.Get(context.expr());

            if (operandValue != DataType.Bool)
            {
                Errors.ReportError(context.NEG().Symbol, "ERROR: Incorrect type in NEG");
                return;
            }

            values.Put(context, DataType.Bool);
            code.Put(context, operand + "not\n");
        }

        bool first = true;
        string firstText = "";

        public override void EnterAssignment([NotNull] ExprParser.AssignmentContext context)
        {
            if (first)
            {
                first = false;
                firstText = context.expr().GetText();
            }
        }

        public override void ExitAssignment([NotNull] ExprParser.AssignmentContext context)
        {
            var right = code.Get(context.expr());
            var rightValue = values.Get(context.expr());

            var variableValue = symbolTable[context.IDENTIFIER().Symbol];
            string variable = context.IDENTIFIER().GetText();

            var sameVariableTypes = variableValue.Type == rightValue ? true : false;


            if (sameVariableTypes)
            {
                symbolTable[context.IDENTIFIER().Symbol] = (rightValue, context.IDENTIFIER().Symbol);
                values.Put(context, rightValue);
                code.Put(context, right + "save " + variable + "\n" + "load " + variable + "\n");
            }
            else
            {
                if (rightValue == DataType.Int && variableValue.Type == DataType.Float)
                {
                    var value = DataType.Float;
                    symbolTable[context.IDENTIFIER().Symbol] = (value, context.IDENTIFIER().Symbol);
                    values.Put(context, value);
                    code.Put(context, right + "itof\n" + "save " + variable + "\n" + "load " + variable + "\n");
                }
                else
                {
                    Errors.ReportError("ERROR: Incorrect Assignment");
                    return;
                }
            }

            if (context.expr().GetText() == firstText && !first)
            {
                first = true;
                right = code.Get(context);
                code.Put(context, right + "pop\n");
            }
        }
    }
}
