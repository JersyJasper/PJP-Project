﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public enum DataType
    {
        Int, Float, Bool, String, Error, Void
    }
}
