﻿grammar Expr;

/** The start rule; begin parsing here. */
program : statement+ EOF;

statement
    : SEMICOLON                                             # emptyStatement
    | '{' statement* '}'                                    # blockOfStatements
    | dataType IDENTIFIER (COMMA IDENTIFIER)* SEMICOLON     # declaration           
    | IF '(' expr ')' pos=statement (ELSE neg=statement)?   # ifElse               
    | WHILE '(' expr ')' statement                          # while                 
    | READ IDENTIFIER (COMMA IDENTIFIER)* SEMICOLON         # readStatement         
    | WRITE expr (COMMA expr)* SEMICOLON                    # writeStatement        
    | expr SEMICOLON                                        # printExpr                   
    ;

expr
    : prefix=SUB expr                       # unaryMinus
    | prefix=NEG expr                       # negation
    | expr op=(MUL|DIV) expr                # mulDiv
    | expr op=MOD expr                      # mod
    | expr op=(ADD|SUB) expr                # addSub
    | expr op=CONCAT expr                   # concat
    | expr op=(LES|GRE) expr                # relation
    | expr op=(EQ|NEQ) expr                 # comparison
    | expr op=(AND|OR) expr                 # logicalAndOr
    | <assoc=right> IDENTIFIER '=' expr     # assignment
    | IDENTIFIER                            # id
    | INT                                   # int
    | FLOAT                                 # float
    | BOOL                                  # bool
    | STRING                                # string
    | '(' expr ')'                          # parentheses
    ;

dataType
    : type=INT_KEYWORD
    | type=FLOAT_KEYWORD
    | type=BOOL_KEYWORD
    | type=STRING_KEYWORD
    ;

INT_KEYWORD : 'int' ;
FLOAT_KEYWORD : 'float' ;
BOOL_KEYWORD : 'bool' ;
STRING_KEYWORD : 'string' ;

SEMICOLON:          ';';
COMMA:              ',';

ADD : '+' ;
SUB : '-' ;
MUL : '*' ; 
DIV : '/' ;

MOD : '%' ;
CONCAT: '.';

LES : '<' ;
GRE : '>' ;
EQ  : '==' ;
NEQ : '!=' ;
AND : '&&' ;
OR : '||' ;
NEG : '!' ;


IF : 'if' ;
ELSE : 'else' ;
WHILE : 'while' ;

READ : 'read' ;
WRITE : 'write' ;

FLOAT : [0-9]+'.'[0-9]+ ;
INT : [0-9]+ ; 
BOOL : ('true'|'false') ;
STRING : '"' ( ESC | ~["\\] )* '"' ;

IDENTIFIER :  [a-zA-Z]+ [a-zA-Z0-9]*;

fragment ESC
    : '\\' [nrtbf"'\\]
    ;

WHITESPACE : [ \t\r\n]+ -> skip ;
SINGLE_LINE_COMMENT : '//' ~[\r\n]* -> skip;